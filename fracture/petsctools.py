# Copyright (C) 2015 Patrick Farrell
#
# This file is a supplemental material to the paper
# Linear and nonlinear solvers for variational phase-field models of brittle fracture
# by P. E. Farrell and C. Maurini
#
# varfrac-solvers is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# varfrac-solvers is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with varfrac-solvers. If not, see <http://www.gnu.org/licenses/>.

import instant
import petsc4py
import os

include_dirs = [os.path.join(os.getenv('PETSC_DIR'), 'include'),
                os.path.join(os.getenv('PETSC_DIR'), os.getenv('PETSC_ARCH'), 'include'),
                petsc4py.get_include(),
                # WARNING: the following paths are manually set.
                # Update them if you have compiling issue for this module
                '/usr/lib/openmpi/include', '/usr/lib/openmpi/include/openmpi',
                '/usr/local/include', '/usr/local/include/openmpi',
                ]
swig_include_dirs = [petsc4py.get_include()]
library_dirs = [os.path.join(os.getenv('PETSC_DIR'), os.getenv('PETSC_ARCH'), 'lib')]
libraries = ['petsc']

# A nice little code that computes the norm of the residual of a nonlinear problem,
# when bound constraints are present.

get_residual_norm_vi_cpp_code = r"""
#ifdef SWIG
%include "petsc4py/petsc4py.i"
#endif

#include <petsc.h>

double get_residual_norm_vi(Vec F,Vec X, Vec XL, Vec XU)
{
  PetscErrorCode    ierr;
  const PetscScalar *x,*xl,*xu,*f;
  PetscInt          i,n;
  PetscReal         rnorm;
  PetscReal         fnorm;

  PetscFunctionBegin;
  ierr  = VecGetLocalSize(X,&n);CHKERRQ(ierr);
  ierr  = VecGetArrayRead(XL,&xl);CHKERRQ(ierr);
  ierr  = VecGetArrayRead(XU,&xu);CHKERRQ(ierr);
  ierr  = VecGetArrayRead(X,&x);CHKERRQ(ierr);
  ierr  = VecGetArrayRead(F,&f);CHKERRQ(ierr);
  rnorm = 0.0;
  for (i=0; i<n; i++) {
    if (((PetscRealPart(x[i]) > PetscRealPart(xl[i]) + 1.e-8 || (PetscRealPart(f[i]) < 0.0)) && ((PetscRealPart(x[i]) < PetscRealPart(xu[i]) - 1.e-8) || PetscRealPart(f[i]) > 0.0))) rnorm += PetscRealPart(PetscConj(f[i])*f[i]);
  }
  ierr   = VecRestoreArrayRead(F,&f);CHKERRQ(ierr);
  ierr   = VecRestoreArrayRead(XL,&xl);CHKERRQ(ierr);
  ierr   = VecRestoreArrayRead(XU,&xu);CHKERRQ(ierr);
  ierr   = VecRestoreArrayRead(X,&x);CHKERRQ(ierr);
  ierr   = MPI_Allreduce(&rnorm,&fnorm,1,MPIU_REAL,MPIU_SUM,PetscObjectComm((PetscObject)F));CHKERRQ(ierr);
  fnorm = PetscSqrtReal(fnorm);
  return fnorm;
}

"""

get_residual_norm_vi =  instant.build_module(code=get_residual_norm_vi_cpp_code,
                                              include_dirs=include_dirs,
                                              library_dirs=library_dirs,
                                              libraries=libraries,
                                              swig_include_dirs=swig_include_dirs
                                              ).get_residual_norm_vi
del get_residual_norm_vi_cpp_code

# A grand little code to set the number of linear iterations used by a SNES.

set_snes_linear_its_cpp_code = r"""
#ifdef SWIG
%include "petsc4py/petsc4py.i"
#endif

#include <petsc.h>
#include <petsc/private/snesimpl.h>

void set_snes_linear_its(SNES snes, PetscInt lits)
{
    snes->linear_its = lits;
}
"""

set_snes_linear_its =  instant.build_module(code=set_snes_linear_its_cpp_code,
                                              include_dirs=include_dirs,
                                              library_dirs=library_dirs,
                                              libraries=libraries,
                                              swig_include_dirs=swig_include_dirs).set_snes_linear_its
del set_snes_linear_its_cpp_code

