# Copyright (C) 2015 Patrick Farrell
#
# This file is a supplemental material to the paper
# Linear and nonlinear solvers for variational phase-field models of brittle fracture
# by P. E. Farrell and C. Maurini
#
# varfrac-solvers is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# varfrac-solvers is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with varfrac-solvers. If not, see <http://www.gnu.org/licenses/>.

from dolfin import *
from utils import empty_vector
from petsc4py import PETSc

class ForwardProblem(NonlinearProblem):
  """The base class for the forward problem.
  Separates details of
   - residual evaluation
   - Jacobian action
   - Jacobian transpose action
   - preconditioner application

  from PETSc. This is so that we can go matrix-free."""

  def __init__(self, F, Y, y, bcs=None, bounds=None, P=None):
    """
    The constructor: takes in the form, function space, solution, and
    boundary conditions.
    """

    assert isinstance(Y, FunctionSpace)
    self.function_space = Y
    self.mesh = Y.mesh()

    NonlinearProblem.__init__(self)
    self.y = y
    self.bcs = bcs

    self._form = F
    self._dF = derivative(F, y)
    self.assembler = SystemAssembler(self._dF, self._form, self.bcs)
    self._J = PETScMatrix()

    self._tmpvec1 = empty_vector(y.vector())
    self._tmpvec2 = empty_vector(y.vector())
    self.residual = empty_vector(y.vector())

    # Sometimes for various problems you want to solve submatrices of the base
    # matrix -- e.g. for active set methods for variational inequalities,
    # and in nonlinear fieldsplits.
    self.eqn_subindices = None
    self.var_subindices = None
    self.inact_subindices = None
    self.pc_prefix = "inner_"

    self.assembler.assemble(self._J)

    # in case a fieldsplit preconditioner is requested, and the blocksize
    # isn't set by dolfin
    self.fieldsplit_is = None

    # the near nullspace of the operator
    self.nullsp = None

    if bounds is not None:
        (lb, ub) = bounds
        self.lb = as_backend_type(lb).vec()
        self.ub = as_backend_type(ub).vec()

    # in case you want to use a different matrix to build
    # a preconditioner.
    self.Pmat = None
    if P is not None:
        self.Pmat = P

  def build_cache(self, x, snes=None, build_preconditioner=True):
    if snes is not None:
        snes_prefix = snes.getOptionsPrefix()
        if snes_prefix is not None:
            if len(snes_prefix) > 0:
                if not self.pc_prefix.startswith(snes_prefix):
                    self.pc_prefix = snes_prefix + "inner_"

    assert (self.y.vector() - x).norm("l2") < 1.0e-12

    self.assembler.assemble(self._J)

    if build_preconditioner:
        self.build_preconditioner()

  def set_nfs_subindices(self, equations, variables):
    self.eqn_subindices = equations
    self.var_subindices = variables

    if equations is not None or variables is not None:
        assert equations is not None
        assert variables is not None
        assert equations.size == variables.size

  def set_inact_subindices(self, inact):
    self.inact_subindices = inact

  def get_submat(self):
    Jmat = mat(self._J)

    if self.eqn_subindices is not None:
        Jmat = Jmat.getSubMatrix(self.eqn_subindices, self.var_subindices)

    if self.inact_subindices is not None:
        Jmat = Jmat.getSubMatrix(self.inact_subindices, self.inact_subindices)

    Pmat = None
    if self.Pmat is not None:
        Pmat = self.Pmat(self.y)
        if self.eqn_subindices is not None:
            Pmat = Pmat.getSubMatrix(self.eqn_subindices, self.var_subindices)

        if self.inact_subindices is not None:
            Pmat = Pmat.getSubMatrix(self.inact_subindices, self.inact_subindices)

    return (Jmat, Pmat)

  def set_pc_prefix(self, prefix):
    if not prefix.endswith("_"):
        prefix = prefix + "_"
    self.pc_prefix = prefix

  def set_fieldsplit_is(self, fieldsplit_is):
    self.fieldsplit_is = fieldsplit_is

  def build_preconditioner(self):
    opts = PETSc.Options().getAll()

    if 'snes_lag_preconditioner' in opts:
      if opts['snes_lag_preconditioner'] == "-1" and hasattr(self, 'P'):
        self.P.setReusePreconditioner(True)
        return

    self.P = PETSc.PC().create()

    # Set the operator appropriately, taking the right subsets
    (Jmat, Pmat) = self.get_submat()
    self.Jmat = Jmat

    if self.nullsp is not None:
        Jmat.setNearNullSpace(self.nullsp)

    self.P.setOperators(Jmat, Pmat)

    (self._ptmpvec1, self._ptmpvec2) = map(PETScVector, Jmat.createVecs())

    self.P.setType("lu")
    self.P.setFactorSolverPackage("mumps")

    self.P.setOptionsPrefix(self.pc_prefix)

    self.P.setFromOptions()

    if self.P.getType() == "fieldsplit" and Jmat.getBlockSize() <= 1 and self.eqn_subindices is None:
        if self.fieldsplit_is is None:
            self.fieldsplit_is = []
            for i in range(self.function_space.num_sub_spaces()):
                subdofs = SubSpace(self.function_space, i).dofmap().dofs()
                iset = PETSc.IS().createGeneral(subdofs)
                self.fieldsplit_is.append(("%s" % i, iset))

        if self.inact_subindices is None:
            self.P.setFieldSplitIS(*self.fieldsplit_is)
        else:
            # OK. Get the dofs from the inactive set and figure out which split they're from.

            fsets = [set(field[1].getIndices()) for field in self.fieldsplit_is]
            inact_fieldsplit_is = {}
            for field in self.fieldsplit_is:
                inact_fieldsplit_is[int(field[0])] = []

            # OK. Suppose you have a 6x6 matrix and the splits are [1, 2, 3, 4]; [5, 6].
            # Suppose further that the inactive indices are [2, 3, 4, 5]. Then what we
            # want to produce for the fieldsplit of the reduced problem is [1, 2, 3]; [4].
            # In other words, we add the *index* of the inactive dof to the reduced split
            # if the inactive dof is in the full split.

            # We also need the offset of how many dofs all earlier processes own.
            from mpi4py import MPI as MPI4
            offset = MPI4.COMM_WORLD.exscan(self.inact_subindices.getLocalSize())
            if offset is None: offset = 0

            for (i, idx) in enumerate(self.inact_subindices.getIndices()):
                for (j, fset) in enumerate(fsets):
                    if idx in fset:
                        inact_fieldsplit_is[j].append(offset + i)
                        break

            inact_input = []
            for j in inact_fieldsplit_is:
                iset = PETSc.IS().createGeneral(inact_fieldsplit_is[j])
                inact_input.append(("%s" % j, iset))

            for (orig_data, vi_data) in zip(self.fieldsplit_is, inact_input):
                orig_iset = orig_data[1]
                vi_iset   = vi_data[1]

                if orig_iset.getSizes() == vi_iset.getSizes():
                    nullsp = orig_iset.query("nearnullspace")
                    if nullsp is not None:
                        vi_iset.compose("nearnullspace", nullsp)

            self.P.setFieldSplitIS(*inact_input)

    if self.eqn_subindices is not None:
        nullsp = self.eqn_subindices.query("nearnullspace")
        if nullsp is not None:
            op = self.P.getOperators()[0]
            op.setNearNullSpace(nullsp)

    self.preconditioner_hook() # argh, such a hack.

    self.P.setUp()

  def preconditioner_hook(self):
    pass

  def F(self, b, x):
    self.assembler.assemble(self._tmpvec1, self.y.vector())

    b.zero()
    b.axpy(1.0, self._tmpvec1)

  def J(self, snes, x, J, P):
    x_wrap = PETScVector(x)
    y_wrap = as_backend_type(self.y.vector())

    x_wrap.update_ghost_values()
    x.copy(y_wrap.vec())
    y_wrap.update_ghost_values()

    self.build_cache(x_wrap, build_preconditioner=False)

  def Jv(self, v, Jonv, x):
    self._J.mult(v, self._tmpvec1)

    Jonv.zero()
    Jonv.axpy(1.0, self._tmpvec1)

  def pc_apply(self, x, y):
    x_wrap = PETScVector(x)
    y_wrap = PETScVector(y)
    self.Pv(x_wrap, y_wrap)

  def Pv(self, v, Ponv):
    self._ptmpvec1.zero()
    self._ptmpvec1.axpy(1.0, v)

    self.P.apply(as_backend_type(self._ptmpvec1).vec(), as_backend_type(self._ptmpvec2).vec())

    Ponv.zero()
    Ponv.axpy(1.0, self._ptmpvec2)

  def set_solver(self, snes):
    self.snes = snes

  def get_solver(self):
    return self.snes

  def unset_solver(self):
    del self.snes

# These functions transfer between FEniCS' wrapper classes around
# Vec and Mat and petsc4py's wrappers around Vec and Mat.
def vec(z):
    if isinstance(z, dolfin.cpp.Function):
        return as_backend_type(z.vector()).vec()
    else:
        return as_backend_type(z).vec()

mat = lambda x: as_backend_type(x).mat()

class VIForwardProblem(ForwardProblem):
    def __init__(self, F, Y, y, bcs=None, bounds=None, P=None):
        ForwardProblem.__init__(self, F, Y, y, bcs, bounds=bounds, P=P)

    def build_cache(self, x, snes=None, build_preconditioner=True):
        ForwardProblem.build_cache(self, x, snes, build_preconditioner)
        self.pc_set_up = False

    def set_solver(self, snes):
        """ Agh. This is where PETSc gets very complicated. We need to know the SNES so that we
            can ask it for its inactive set and create the right preconditioner. Now, if you
            just have one SNES of type VINEWTON?SLS, then it's straightforward. But what about
            composition? What if you have a SNESCOMPOSITE and it's one of the subsneses that knows
            your inactive set? What if it's a nonlinear preconditioner? So here we have to
            go looking. """

        self.snes = snes

        if snes.getType() == "composite":
            for i in range(snes.getCompositeNumber()):
                subsnes = snes.getCompositeSNES(i)
                if subsnes.getType().startswith("vi"):
                    self.snes = subsnes

    def build_preconditioner(self):
        # this routine is called by ForwardProblem.build_cache,
        # but we can't build the preconditioner at cache time
        # because we need the active set. So we do it ourselves inside Pv.
        pass

    def _build_preconditioner(self):
        if self.snes.getType().startswith("vi"):
            inact = self.snes.getVIInactiveSet()
            self.set_inact_subindices(inact)
        else:
            self.set_inact_subindices(None)

        ForwardProblem.build_preconditioner(self)
        self.pc_set_up = True

    def Pv(self, v, Ponv):
        x = vec(v)

        if not self.pc_set_up:
            self._build_preconditioner()

        y = vec(Ponv)

        assert x.size == y.size
        if self.inact_subindices is not None:
            assert x.size == self.inact_subindices.size

        self.P.apply(x, y)

