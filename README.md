# varfrac-solvers

### Description

varfrac-solvers is distributed as supplemental material to the paper

*Linear and nonlinear solvers for variational phase-field models of brittle fracture*, P. E. Farrell and C. Maurini


The pdf of the paper can be downloaded here http://onlinelibrary.wiley.com/doi/10.1002/nme.5300/full in Open Access. 

This repository is distributed to provide a minimal working implementation and examples of the solvers discussed in the paper.

### Citing

If you use varfrac-solvers in your work we would ask that you consider citing our paper.

```
@article {NME:NME5300,
author = {Farrell, Patrick and Maurini, Corrado},
title = {Linear and nonlinear solvers for variational phase-field models of brittle fracture},
journal = {International Journal for Numerical Methods in Engineering},
volume = {10
number = {5},
issn = {1097-0207},
url = {http://dx.doi.org/10.1002/nme.5300},
doi = {10.1002/nme.5300},
pages = {648--667},
keywords = {fracture, damage, variational methods, phase-field, nonlinear Gauss�Seidel, Newton's method},
year = {2017},
note = {nme.5300},
}
```

### Contents

   * The directory `examples` contains a working example (the thermal shock problem reported in the paper).

   * The directory `fracture` contains the basic solver source files.


### Getting started


   * To run the example you should have the following environment variables set:

    ```
    $PETSC_DIR, $PETSC_ARCH, $MPI_HOME
    ```

   * You may need to add the directory of your MPI headers to the `include_dirs` variable
     in fracture/petsctools.py.

   * Run the example:

    ```
    cd examples/thermal-shock
    python thermal-shock.py
    ```

   * The default parameters are set to run a full simulation in a relatively short time.
     The key parameters may be set at the command line.
      - The simulation in Figure of the paper can be reproduced with the command-line

        ```
        python thermal-shock.py --user.L 40 --user.H 10 --user.tmax 5. --user.intensity 4.0 --user.overrelaxation 1.0
        ```
      - To get a list of available user parameters, run with --help
      - To test the different solvers proposed in the paper you can run with

        ```
        python thermal-shock.py --petsc_args args
        ```
        where ```args``` can be selected among the following values (ORAM and ORAM-N refer to the algorithms presented in the paper)

         - `am_lu_args` (ORAM with ideal linear solver)
         - `am_ksp_args` (ORAM with iterative linear solver)
         - `am_newton_lu_args` (ORAM-N with ideal solvers)
         - `am_newton_ksp_ideal_args` (ORAM-N with iterative outer solver and ideal inner solvers),
         - `am_newton_ksp_args` (ORAM-N with fully iterative solvers)

        The solver parameters are set through the PETSc command line parameters available in `fracture/petscsetting.py`


### Dependencies

The code has been tested against the following git revisions of PETSc and FEniCS.

To run the code you can follow one of the following possibilities
   
   1. Compile the required version of PETSc and FEniCS on your own by following the instructions [here](http://fenicsproject.org/download).

   2. Use the provided vagrant/virtualbox virtual machine:
       - Install vagrant [https://www.vagrantup.com]
       
       - Go to a working directory, download and start the vagrant/virtualbox virtual machine by typing at the command line (attention it needs to download about 3 GB)
        
        ```
        vagrant init cmaurini/fenics-dev-1.7; vagrant up --provider virtualbox
        ```
      
       - Clone this repository and enter the virtual machine
       
        ```
        git clone https://bitbucket.org/pefarrell/varfrac-solvers.git
        ```
        
        ```
        vagrant ssh
        ```
         
        The current working directory will be shared in the `/vagrant` directory in the virtual machine.
        The username and password in the virtual machine are both `vagrant`
         
       - You have now a terminal run by the virtual machine and you can play with the example:
       
        ```
        cd /vagrant/varfrac-solvers/examples/thermal-shock
        ```
      
        ```
        python thermal-shock.py
        ```
   
   3. Use the docker virtual machine provided in the fenics website. Hence use the code in the branch `docker-fenics-2016.2.0`. 


         

PETSc/SLEPc's components

  * petsc:
      - git: 1c4573aa0175145fced24f37de9c0ce8d7d5a801
      - url: https://bitbucket.org/petsc/petsc.git
  * petsc4py:
      - git: a587c138a249a07fab85eb9e66f84c37fa6cd18c
      - url: https://bitbucket.org/petsc/petsc4py.git
  * slepc:
      - git: 77a7748d2ff71dd86d23fd7756f2a0b727c41b02
      - url: https://bitbucket.org/slepc/slepc.git
  * slepc4py:
      - git: 56890f198f1e011816f7773d3b6e50dc8644cb83
      - url: https://bitbucket.org/slepc/slepc4py.git


FEniCS's components:

  * ffc:
      - git: git:df31b8ec66a240abf3d98a18b85a539210f7fdb7
      - url: https://bitbucket.org/fenics-project/ffc.git
  * fiat:
      - git: git:5b7f77abcea7d7e9b67b597a32543a12547ddf9b
      - url: https://bitbucket.org/fenics-project/fiat.git
  * instant:
      - git: git:b9411d8ea1bd2f140dd11c24c7470d838a1db397
      - url: https://bitbucket.org/fenics-project/instant.git
  * ufl:
    sources:
      - git: f40cf5b3ff3be6c025980c0d4f0596153404214a
      - url: https://bitbucket.org/fenics-project/ufl.git
  * uflacs:
      - git: cf3b6d0b098fd5376d18427ed75c282dfabb25ea
      - url: https://bitbucket.org/fenics-project/uflacs.git
  * dolfin
      - key: git:8ad7544e62714eaf561e52880925d3713756f3c0
      - url: https://bitbucket.org/fenics-project/dolfin.git
  * mshr:
      - git: 8577b3b4ffbe43b0eb06fb249a9e4f5bd43340da
      - url: https://bitbucket.org/fenics-project/mshr.git


### Issues and Support

Please use the [bugtracker](http://bitbucket.org/pefarrell/varfrac-solvers)
to report any issue.

For support or questions please email [corrado.maurini@upmc.fr](mailto:corrado.maurini@upmc.fr).

### Authors

* Patrick Farrell, Mathematical Institute, University of Oxford, Oxford, UK and Center for Biomedical Computing, Simula Research Laboratory, Oslo, Norway

* Corrado Maurini, Institut Jean Le Rond d'Alembert,  Sorbonne Universit\'es, UPMC, Univ Paris 06, CNRS, UMR 7190, France

### License

varfrac-solvers is free software: you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License along
with fenics-shells.  If not, see <http://www.gnu.org/licenses/>.
